#include <stdio.h>
#include <unistd.h>

#include "../include/file_access.h"

bool exists_file_check(const char* path) {
    return access(path, F_OK) == 0;
}

bool read_access_file_check(const char* path) {
    return access(path, R_OK) == 0;
}

bool write_access_file_check(const char* path) {
    return access(path, W_OK) == 0;
}

FILE* open_binary_readonly(const char* path) {
    return fopen(path, "rb"); 
}

FILE* create_if_hadnt_and_open_binary_writeonly(const char* path) {
    return fopen(path, "wb+"); 
}

