#include "../../include/image.h"
#include "../../include/transform/image_transform_util.h"

void apply_effect_to_every_pixel(Image* image, void (*apply_effect)(Image* image, size_t y, size_t x)) {
    for (size_t y = 0; y < image->height; y++) {
        for (size_t x = 0; x < image->width; x++) {
            apply_effect(image, y, x);
        } 
    }
}