#include "../../include/image.h"
#include "../../include/transform/image_rotate.h"

void rotate_image(Image* image_to_rotate, Image* image_res) {
    setup_empty_image(image_res, image_to_rotate->width, image_to_rotate->height);
    for (size_t w = 0; w < image_to_rotate->width; w++) {
        for (size_t h = 0; h < image_to_rotate->height; h++) {
            image_res->data[w][h] = image_to_rotate->data[h][w];
        }
    }
}
