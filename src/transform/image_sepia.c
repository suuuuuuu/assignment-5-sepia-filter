#include "../../include/image.h"
#include "../../include/transform/image_serpia.h"
#include "../../include/transform/image_transform_util.h"
#include <stdint.h>


static void apply_serpia_to_image_pixel(Image* image, size_t y, size_t x);
static void apply_serpia_to_pixel(Pixel* pixel);

void apply_serpia_to_image(Image* image) {
    apply_effect_to_every_pixel(image, apply_serpia_to_image_pixel);
}

static void apply_serpia_to_image_pixel(Image* image, size_t y, size_t x) {
    apply_serpia_to_pixel(&image->data[y][x]);
}

// source: https://dyclassroom.com/image-processing-project/how-to-convert-a-color-image-into-sepia-image 
static void apply_serpia_to_pixel(Pixel* pixel) {
    // tr = 0.393R + 0.769G + 0.189B
    // tg = 0.349R + 0.686G + 0.168B
    // tb = 0.272R + 0.534G + 0.131B
    uint16_t serpia_red = (pixel->red * .393) + (pixel->green *.769) + (pixel->blue * .189);
    uint16_t serpia_green = (pixel->red * .349) + (pixel->green *.686) + (pixel->blue * .168);
    uint16_t serpia_blue = (pixel->red * .272) + (pixel->green *.534) + (pixel->blue * .131);
    // If tr > 255 then r = 255 else r = tr
    // If tg > 255 then g = 255 else g = tg
    // If tb > 255 then b = 255 else b = tb 
    pixel->red = serpia_red > 255 ? 255 : serpia_red;
    pixel->green = serpia_green > 255 ? 255 : serpia_green;
    pixel->blue = serpia_blue > 255 ? 255 : serpia_blue;
}