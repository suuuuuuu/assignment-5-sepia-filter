#include "../include/bmp_writer.h"
#include "../include/image.h"
#include "../include/bmp_structs.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static enum write_status write_header(FILE*, struct bmp_header);
static enum write_status write_image(FILE*, const Image*);
static struct bmp_pixel* get_bmp_pixel_row(Pixel* pixel_row, size_t row_size);

enum write_status write_image_to_bmp(FILE* bmp_file_output, const Image* image) {
    struct bmp_header header = init_40b_bmp_header(image->height, image->width);
    enum write_status header_write_status = write_header(bmp_file_output, header);
    if (header_write_status != WRITE_OK) {
        return header_write_status;
    }
    enum write_status image_write_status = write_image(bmp_file_output, image);
    return image_write_status;
}


static enum write_status write_header(FILE* bmp_file_output, struct bmp_header header) {
    size_t wrote_count = fwrite(&header, sizeof(struct bmp_header), 1, bmp_file_output);
    if (wrote_count != 1) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}
static enum write_status write_image(FILE* bmp_file_output, const Image* image) {
    const size_t END_PADDING = get_bmp_24_padding(image->width); 
    for (size_t h = 0; h < image->height; h++) {
        struct bmp_pixel* pixel_row = get_bmp_pixel_row(image->data[h], image->width);
        size_t wrote_count = fwrite(pixel_row, sizeof(struct bmp_pixel), image->width, bmp_file_output);
        free(pixel_row);
        if (wrote_count != image->width) {
            return WRITE_ERROR;
        }
        size_t skip_padding_res = fseek(bmp_file_output, END_PADDING, SEEK_CUR); 
        if (skip_padding_res != 0) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

static struct bmp_pixel* get_bmp_pixel_row(Pixel* pixel_row, size_t row_size) {
    struct bmp_pixel* bmp_pixel_row = calloc(sizeof(struct bmp_pixel), row_size);
    for (size_t w = 0; w < row_size; w++) {
        bmp_pixel_row[w] = pixel_to_bmp_pixel(pixel_row[w]);
    }
    return bmp_pixel_row;
}
