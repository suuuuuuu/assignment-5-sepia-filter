#include <stdio.h>
#include <string.h>
#include <unistd.h>

// добавить -I в make
#include "../include/file_access.h"
#include "../include/image.h"
#include "../include/bmp_reader.h"
#include "../include/bmp_writer.h"
#include "../include/transform/all_transforms.h"
#include "../include/transform/image_transform_util.h"

static void print_wrong_args_count_msg(const int args_count);
static enum TransformationType determine_transformation_type(char* transformation_type);
static void print_input_file_existance_check_failed_msg(char* in_file_path);
static void print_input_file_read_access_failed_msg(char* in_file_path);
static void print_output_file_exists_write_access_failed_msg(char* in_file_path);
static void print_bad_read_status_msg(enum read_status);
static void print_bad_write_status_msg(enum write_status);
static void print_transformed_write_success_msg(enum TransformationType transformation_type);
static void print_rotated_write_success_msg(void);
static void print_serpia_applied_write_success_msg(void);
static void print_wrong_transformation_type_msg(char* transformation_type_str);
static void print_fatal_error(void);

int main(int argc, char** argv) {
    if (argc != 4) { // first([1] ofc) argument is transformation type
        print_wrong_args_count_msg(argc);
        return 1;
    }
    char* transformation_type_str = argv[1];
    char* in_file_path = argv[2];
    char* out_file_path = argv[3];
    enum TransformationType transformation_type = determine_transformation_type(transformation_type_str);        
    if (transformation_type == TR_UNDEFINED) {
        print_wrong_transformation_type_msg(transformation_type_str);
        return 1;
    }
    if (!exists_file_check(in_file_path)) {
        print_input_file_existance_check_failed_msg(in_file_path);
        return 1;
    }
    if (!read_access_file_check(in_file_path)) {
        print_input_file_read_access_failed_msg(in_file_path);
        return 1;
    }
    if (exists_file_check(out_file_path) && !write_access_file_check(out_file_path)) {
        print_output_file_exists_write_access_failed_msg(out_file_path);
        return 1;
    }
    FILE* in_file = open_binary_readonly(in_file_path);
    Image read_image = init_empty_image(); 
    enum read_status image_read_status = read_image_from_bmp(in_file, &read_image);
    fclose(in_file);
    if (image_read_status != READ_OK) {
        print_bad_read_status_msg(image_read_status);
        return 1;
    }
    Image res_image = init_empty_image();
    switch (transformation_type) {
        case TR_ROTATE:
            rotate_image(&read_image, &res_image);
            free_image(&read_image);
            break;
        case TR_SERPIA:
            apply_serpia_to_image(&read_image);
            res_image = read_image;
            break;
        default:
            print_fatal_error();
            return 1;
    }
    FILE* out_file = create_if_hadnt_and_open_binary_writeonly(out_file_path);
    enum write_status image_write_status = write_image_to_bmp(out_file, &res_image);
    fclose(out_file);
    free_image(&res_image);
    if (image_write_status != WRITE_OK) {
        print_bad_write_status_msg(image_write_status);
        return 1;
    }
    print_transformed_write_success_msg(transformation_type);
    return 0;
}


static void print_wrong_transformation_type_msg(char* transformation_type_str) {
    printf("Первым аргументом вы ввели неверный тип трасфорации\n"
        "Он может принимать только следующие значения: \"serpia\" или \"rotate\".\n"
        "Введенный тип трансформации: %s",
        transformation_type_str);
}

static enum TransformationType determine_transformation_type(char* transformation_type_str) {
    if (strcmp(transformation_type_str, "rotate") == 0) {
        return TR_ROTATE;
    } 
    if (strcmp(transformation_type_str, "serpia") == 0) {
        return TR_SERPIA;
    } 
    return TR_UNDEFINED;
}

static void print_wrong_args_count_msg(const int args_count) {
    printf("Первым должен идти тип трансформации (\"serpia\" или \"rotate\").\n"
        "Вторым должен быть путь файла для переворота.\n"
        "Третьим - путь до выходного файла.\n"
        "Вы же указали %d аргумента(-ов) вместо требуемых трех.",
        args_count - 1);
}

static void print_input_file_existance_check_failed_msg(char* in_file_path) {
    printf("Путь файла для переворота (2-ый аргумент) неверный.\n"
        "Введенный путь: %s",
        in_file_path);
}

static void print_input_file_read_access_failed_msg(char* in_file_path) {
    printf("Файл для переворота (2-ый аргумент) запрещает доступ на чтение.\n"
        "Его путь: %s",
        in_file_path);
}

static void print_output_file_exists_write_access_failed_msg(char* out_file_path) {
    printf("Выходной файл (3-ой аргумент) существует, но запрещает доступ на запись.\n"
        "Перенесите файл куда-нибудь, если он важен, или просто удалите его.\n"
        "Его путь: %s",
        out_file_path);
}

static void print_bad_read_status_msg(enum read_status input_read_status) {
    switch (input_read_status) {
        case READ_END_OF_FILE_NOT_EXPECTED: 
            printf("При чтении часть файла ожидалась, но ее не оказалось на месте - файл закончился");
            break;
        case READ_FATAL_ERROR:
            printf("Фатальная ошибка чтения");
            break;
        case READ_BAD_RESOLUTION_IN_HEADER:
            printf("В хедере неверное разрешение картинки");
            break;
        case READ_UNSUPPORTED_COLOR_PALETE:
            printf("Программа не поддерживает глубины цвета, отличные от 24 бит");
            break;
        default:
            break;
    }
}

static void print_bad_write_status_msg(enum write_status input_write_status) {
    switch (input_write_status) {
        case WRITE_ERROR:
            printf("Ошибка записи в файл");
            break;
        default:
            break;
    }
}

static void print_transformed_write_success_msg(enum TransformationType transformation_type) {
    switch (transformation_type) {
        case TR_ROTATE:
            print_rotated_write_success_msg();
            break;
        case TR_SERPIA:
            print_serpia_applied_write_success_msg();
            break;
        case TR_UNDEFINED:
            print_fatal_error();
            break;
    }
}

static void print_rotated_write_success_msg(void) {
    printf("Картинка перевернута и успешно записана в выходной файл");
}

static void print_serpia_applied_write_success_msg(void) {
    printf("К картинке применен фильр серпиа, и она успешно записана в выходной файл");
}

static void print_fatal_error(void) {
    printf("Фатальная ошибка - программист дятел");
}