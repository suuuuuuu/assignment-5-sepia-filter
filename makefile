CC=gcc
CFLAGS=
DEPS = include/%.h
OBJS = src/*.c src/transform/*.c

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

transform/%.o: transform/%.c
	mkdir $($@)
	$(CC) -c $< -o $@

main: $(OBJS)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm $(OBJS)
