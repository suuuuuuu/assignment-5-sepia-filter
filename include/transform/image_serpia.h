#ifndef IMAGE_TRANSFORM_H 
#define IMAGE_TRANSFORM_H

#include "../image.h"

void apply_serpia_to_image(Image* image);

#endif
