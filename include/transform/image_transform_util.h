#ifndef IMAGE_TRANSFORM_UTIL_H 
#define IMAGE_TRANSFORM_UTIL_H

#include "../image.h"

void apply_effect_to_every_pixel(Image* image, void (*apply_effect)(Image* image, size_t y, size_t x));

enum TransformationType {
    TR_ROTATE,
    TR_SERPIA,
    TR_UNDEFINED
};

#endif
