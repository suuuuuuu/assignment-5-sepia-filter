#ifndef IMAGE_ROTATE_H 
#define IMAGE_ROTATE_H

#include "../image.h"

void rotate_image(Image* image_to_rotate, Image* image_res);

#endif
