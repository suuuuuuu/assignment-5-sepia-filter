#ifndef IMAGE_H 
#define IMAGE_H

#include <stddef.h>
#include  <stdint.h>

typedef struct image {
  uint64_t width, height;
  struct pixel** data;
} Image;

// представляется в формате RGB
typedef struct pixel { 
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} Pixel;

void free_image(Image* image);

void setup_empty_image(Image* image, int64_t height, int64_t width);

Image init_image(int64_t height, int64_t width);

Image init_empty_image(void);

Pixel** init_empty_pixel_map(int64_t height, int64_t width);

void free_pixel_map(Pixel** data, size_t height);

#endif
